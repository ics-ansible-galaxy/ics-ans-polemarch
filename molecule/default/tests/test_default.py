import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('polemarch')


def test_redis_file(host):
    f = host.file('/etc/redis.conf')
    assert f.exists


def test_mariadb_file(host):
    f = host.file('/etc/my.cnf')
    assert f.exists


def test_polemarch_file(host):
    f = host.file('/etc/polemarch/settings.ini')
    assert f.exists


def test_redis_enabled_and_running(host):
    service = host.service('redis')
    assert service.is_running
    assert service.is_enabled


def test_mariadb_enabled_and_running(host):
    service = host.service('mariadb')
    assert service.is_running
    assert service.is_enabled


def test_polemarch_port(host):
    socket = host.socket('tcp://0.0.0.0:8080')
    assert socket.is_listening
